# Game Jam Website

This is the website with all information around the Game Jam and how to participate.

It is minimalistic not only in its look, but also its programming – and that is by design.
To update information, only that part in the HTML has to be changed.
No need for re-compiling, complex toolchains and struggling with dependencies:
This is 100% hand-made, with :heart:
